package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.servlets;

import nl.bioinf.ahclugtenberg.Webbased_DNATranslator.translator.AcidSequence;
import nl.bioinf.ahclugtenberg.Webbased_DNATranslator.translator.TranslateNucs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Anouk Lugtenberg on 10-1-2018.
 * Servlet which handles the translation of the DNA sequence.
 * Input is a sequence, output is either a list with acid sequences or an error message
 */
@WebServlet(name = "DNATranslateServlet", urlPatterns = "/translateDNA.do")
public class DNATranslateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AcidSequence> allAcidSequences = new ArrayList<>();
        String sequence = request.getParameter("sequence");
        int minimumORF = Integer.parseInt(request.getParameter("minORF"));

        request.setAttribute("sequence", sequence);

        String frames[] = request.getParameterValues("frames");

        //Checks if checkboxes with frames are checked by the user, otherwise error message is set
        if (frames != null) {
             //If 6 frame translation is selected (value is 6), all other frames are put in the frame array
            if (Arrays.asList(frames).contains("6")) {
                frames = new String[]{"1", "2", "3", "-1", "-2", "-3"};
            }
            for (String frame : frames) {
                int value = Integer.parseInt(frame); //Converts String to int
                TranslateNucs tn = new TranslateNucs(sequence, value, minimumORF);
                if (!tn.getAcidSequencesList().isEmpty()) {
                    allAcidSequences.addAll(tn.getAcidSequencesList());
                } else {
                    request.setAttribute("errorMessage", tn.getErrorMessage());
                }
            }
        } else {
            request.setAttribute("errorMessage", "Please select which frames you want to translate.");
        }

        //If list is not empty, list of acid sequences is returned
        if (!allAcidSequences.isEmpty()) {
            request.setAttribute("acidSequenceList", allAcidSequences);
        }

        RequestDispatcher view = request.getRequestDispatcher("translate.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
