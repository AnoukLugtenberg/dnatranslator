package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.servlets;

import nl.bioinf.ahclugtenberg.Webbased_DNATranslator.dao.MyDbConnector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Anouk Lugtenberg on 10-1-2018.
 * Handles the creation of new users, and puts them in a database.
 */
@WebServlet(name = "CreateNewUserServlet", urlPatterns = "/createUser.do")
public class CreateNewUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String viewJsp = "index.jsp";
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        //Connection to the database is made.
        MyDbConnector mdc = new MyDbConnector();
        mdc.connect();

        //Checks if username is already existing in the database
        if (mdc.checkExistingUsername(userName)) {
            request.setAttribute("errorMessage", "This username already exists. Please try again");
            viewJsp = "createUser.jsp";
        } else {
            mdc.insertUser(userName, password, email);
            request.setAttribute("message", "Account creation succesfull, please log in");
        }

        mdc.disconnect();

        RequestDispatcher view = request.getRequestDispatcher(viewJsp);
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
