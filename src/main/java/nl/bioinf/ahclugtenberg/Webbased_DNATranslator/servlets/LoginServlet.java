package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.servlets;

import nl.bioinf.ahclugtenberg.Webbased_DNATranslator.dao.MyDbConnector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ahclugtenberg on 8-1-18.
 * Servlet which handles the log-in of users.
 * Checks if the user exists in the database, if not re-directs the user to the log-in page.
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String viewJsp = "translate.jsp";
        HttpSession session = request.getSession();

        //Connection to database is made
        MyDbConnector mdc = new MyDbConnector();
        mdc.connect();

        //Searches for the user in the database
        if (mdc.checkExistingUser(userName, password)) {
            session.setAttribute("userName", userName);
        } else {
            request.setAttribute("message", "Sorry your credentials are not recognized, please try again.");
            viewJsp = "index.jsp";
        }

        mdc.disconnect();

        RequestDispatcher view = request.getRequestDispatcher(viewJsp);
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
