package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.translator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anouk Lugtenberg on 10-1-2018.
 * Enum to hold the different codons which code for the different proteins
 */
public enum AminoAcids {
    PHE("TTT", "TTC"),
    LEU("CTT", "CTC", "CTA", "CTG", "TTA", "TTG"),
    ILE("ATT", "ATC", "ATA"),
    MET("ATG"), // also the start codon
    VAL("GTT", "GTC", "GTA", "GTG"),
    SER("TCT", "TCC", "TCA", "TCG", "AGT", "AGC"),
    PRO("CCT", "CCC", "CCA", "CCG"),
    THR("ACT", "ACC", "ACA", "ACG"),
    ALA("GCT", "GCC", "GCA", "GCG"),
    TYR("TAT", "TAC"),
    HIS("CAT", "CAC"),
    GLN("CAA", "CAG"),
    ASN("AAT", "AAC"),
    LYS("AAA", "AAG"),
    ASP("GAT", "GAC"),
    GLU("GAA", "GAG"),
    CYS("TGT", "TGC"),
    TRP("TGG"),
    ARG("CGT", "CGC", "CGA", "CGG", "AGA", "AGG"),
    GLY("GGT", "GGC", "GGA", "GGG"),
    STOP("TAA", "TAG", "TGA");

    private List<String> codons;

    AminoAcids(String... codons) {
        this.codons = new ArrayList<String>();
        Collections.addAll(this.codons, codons);
    }

    public String getName() {
        return this.name();
    }

    public List<String> getCodons() {
        return codons;
    }
}

