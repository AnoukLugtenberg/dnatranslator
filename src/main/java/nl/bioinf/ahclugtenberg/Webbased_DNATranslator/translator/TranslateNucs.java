package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.translator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anouk Lugtenberg on 10-1-2018.
 * Class used to translate nucleotides to protein sequences
 */
public class TranslateNucs {
    private String nucSeq;
    private int frame;
    private int minimumOpenReadingFrame;
    private String errorMessage;
    private List<AcidSequence> acidSequencesList = new ArrayList<>();
    private static Map<String, AminoAcids> codons = new HashMap<>();

    /**
     * Constructor for the sequence to be translated
     * @param seq the sequence to be translated
     */
    public TranslateNucs(String seq, int frame, int minimumOpenReadingFrame){
        this.nucSeq = seq;
        this.frame = frame;
        this.minimumOpenReadingFrame = minimumOpenReadingFrame;
        isValidSequence();
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public List<AcidSequence> getAcidSequencesList() {
        return this.acidSequencesList;
    }

    /**
     * Checks if the given sequence is a valid sequence, e.g. only contains A, C, G, T, U
     * When U is found in the sequence, RNA must be translated to DNA first before the translation can be given.
     * When frame is smaller than 0, sequence is reversed
     * If another character is found, an errormessage will be printed to the user.
     */
    private void isValidSequence() {
        //Sets the nucleotides in uppercase, if user provided lowercase.
        System.out.println("this.nucSeq = " + this.nucSeq);
        //TODO IS NOT REPLACING NEW LINES AND WHITESPACES WHEN DONE FROM SERVER.
        this.nucSeq = this.nucSeq.toUpperCase().trim().replaceAll("\n", "");
        System.out.println("nucSeq = " + nucSeq);
        if (this.nucSeq.matches("[ACGTU]*")) {
            if (this.nucSeq.matches("(.*)U(.*)")) {
                translateRNAtoDNA();
            }
            if (this.frame < 0) {
                reverseSequence();
            }
            findStartCodon();
        } else {
            setErrorMessage("This is not a valid RNA/DNA Sequence");
        }
    }

    /**
     * Replaces the 'U' from RNA with the 'G' from DNA.
     */
    private void translateRNAtoDNA() {
        this.nucSeq = this.nucSeq.replace("U", "T");
    }

    /**
     * Reverses the sequence to search in 3 -> 5 way.
     */
    private void reverseSequence() {
        this.nucSeq = new StringBuilder(this.nucSeq).reverse().toString();
    }

    /**
     * Finds the start codons (AUG) in the sequence via regex, when start codon is found
     * translation starts at that spot in the sequence via method startTranslation
     * When no start codon is found, warning message is printed to the user
     */
    private void findStartCodon() {
        for (AminoAcids aminoAcids : AminoAcids.values()) {
            for (String base : aminoAcids.getCodons()) {
                codons.put(base, aminoAcids);
            }
        }

        //Sets this error message, will be changed when a start codon is found
        setErrorMessage("No start codon was found");

        //This.frame - 1, because indexes start at 0 instead of 1
        //Math.abs to set negative frames to positive frames, for reversed sequences
        for (int startCodon = Math.abs(this.frame) - 1; startCodon < (this.nucSeq.length() - 2); startCodon += 3) {
            AminoAcids acid = codons.get(this.nucSeq.substring(startCodon, startCodon + 3));
            //Checks if codon is start codon, if it is starts translation
            if (acid == AminoAcids.MET) {
                setErrorMessage(null); //Changes error message back to null, because a start codon is found
                startTranslation(startCodon); //Translation should begin at index of start codon
            }
        }
    }

    /**
     * Starts the translation from the place of the start codon.
     * Will terminate either when a stop codon (TAA(UAA)/TAG(UAG)/TGA(UGA) is found, or when there's no more sequence to be translated.
     */
    private void startTranslation(int indexStartCodon) {
        StringBuilder acidSeq = new StringBuilder();
        int indexStopCodon = 0; //variable to keep track of the index of the stop codon
        //sequence.length - 2, so IndexOutOfBound is avoided -> since codons are three in length
        for (int i = indexStartCodon; i < (this.nucSeq.length() - 2); i += 3) {
            indexStopCodon = i; //stopcodon is the same index as variable i is
            if (acidSeq.length() != 0) {
                acidSeq.append("-");
            }
            AminoAcids acid = codons.get(this.nucSeq.substring(i, i + 3));
            acidSeq.append(acid.getName());

            //Breaks out of the loop when a stop codon is encountered
            //Starts the search for a start codon again
            if (acid == AminoAcids.STOP) {
                break;
            }

        }

        //If reading frame is more than given lenght, sequence is added to the list.
        if (checkReadingFrameLength(indexStartCodon, indexStopCodon)) {
            AcidSequence as = new AcidSequence();
            as.setIndexStartCodon(indexStartCodon);
            as.setIndexStopCodon(indexStopCodon);
            as.setFrame(this.frame);
            as.setProteinSequence(acidSeq.toString());
            this.acidSequencesList.add(as);
        } else {
            setErrorMessage("None of the translations in this frame has the required open reading frame length.");
        }
    }

    /**
     * Checks if translated sequence is longer than the minimum Open reading frame length
     * @param start start index of the translated sequence
     * @param stop stop index of the translated sequence
     * @return boolean, false if not longer, true if longer
     */
    private boolean checkReadingFrameLength(int start, int stop) {
        return (stop - start) >= this.minimumOpenReadingFrame;
    }

    //Main to test the back-end without having to use the server.
    public static void main(String[] args) {
        TranslateNucs tn = new TranslateNucs("ggccgcgagctattcgaagcaagttaccgcatcgccccatcactacgctagatatcgtta\n" +
                "aagagcgcccaccaacaccgtgtaaagcagcctacaccctacctaatccaatcgagagaa\n" +
                "gatgagcgatgatcttcgagatgcgtctgtacaatacaatcacatcgtgtgacttattat\n" +
                "aagcattttgccttgagatctgaatgctcgttagaaaaaaaacatacgcgagcactgtag\n" +
                "gtcccccgatcaggttccatgaattgttatatgtaccgtctccaaccgtgctacgggata\n" +
                "tattgtggctgaagttgttcgaaagatctcgatagaactacttgggtacaattgccagta\n" +
                "atgagtggagccggggaaaagtgactgacggacggtctgtcttccgagacggtactaacg\n" +
                "tggtcgcggataaagcacccgtgtgcctgtgcacctccaaggagtttggccccgattatg\n" +
                "cagtaggagttaaggctcgtgaccgcctacaaatctttgcgagtggggcatcgttgctcg\n" +
                "cgacatcgagctccgattgaataggtgtgtttaagtggtgctccaagagaaggtcaagga\n" +
                "ggtctaacgcaatcgttcgcagtatcacctaatggacaatgtagtcccctccttagtgaa\n" +
                "gaacgcgcacatttcccatccttttagaacgtcgaaaattgaaaccttgcagcttatacg\n" +
                "tcgcagtgtaagtcgtccgtgtttcagatcaactgagtcagaaaacgctggcaacgtgac\n" +
                "ataggaactaatggcggagtgtccactaagtccaccaagttatagtcaatccaacgcgtg\n" +
                "cctaagtgcctaattgggcaggtggacgccagtatgcattttttgaaattgaggtataag\n" +
                "gccccatgggattgagacgttgcctttcatacggtactcacagcttagtaataaccgtcg\n" +
                "tcacgcatacgttaggggtccagcataagcactaagatgt", 1, 6);
        System.out.println("tn.getErrorMessage() = " + tn.getErrorMessage());
        for (AcidSequence i : tn.getAcidSequencesList()) {
            System.out.println(i.getProteinSequence());
        }
    }
}

