package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.translator;

/**
 * Created by Anouk Lugtenberg on 11-1-2018.
 * Used to create objects of acid sequences
 */
public class AcidSequence {
    private String proteinSequence;
    private int indexStartCodon;
    private int indexStopCodon;
    private int frame;

    //Has to be public because it's used in the translate.jsp page
    public String getProteinSequence() {
        return proteinSequence;
    }

    void setProteinSequence(String proteinSequence) {
        this.proteinSequence = proteinSequence;
    }

    //Is used in translate.jsp page
    public int getIndexStartCodon() {
        return indexStartCodon;
    }

    void setIndexStartCodon(int indexStartCodon) {
        this.indexStartCodon = indexStartCodon;
    }

    //Is used in translate.jsp page
    public int getIndexStopCodon() {
        return indexStopCodon;
    }

    void setIndexStopCodon(int indexStopCodon) {
        this.indexStopCodon = indexStopCodon;
    }

    public int getFrame()  {return frame; }

    void setFrame(int frame) { this.frame = frame; }

}
