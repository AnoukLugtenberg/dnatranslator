package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.dao;

/**
 * Created by ahclugtenberg on 8-1-18.
 * Interface for the database
 */
public interface MyAppDao {
    void connect() throws ClassNotFoundException;
    boolean checkExistingUser(String userName, String password);
    boolean checkExistingUsername(String userName);
    void insertUser(String userName, String userPassword, String email);
    public void disconnect();
}
