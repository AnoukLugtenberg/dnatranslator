package nl.bioinf.ahclugtenberg.Webbased_DNATranslator.dao;

import java.sql.*;

/**
 * Created by ahclugtenberg on 8-1-18.
 * Controls the connection to the database.
 * Also contains 3 prepared statements.
 */

public class MyDbConnector implements MyAppDao {
        private Connection connection;

    /**
     * Connects to the database
     */
    public void connect() {
        String dbUser = "ahclugtenberg";
        String dbPassword = "a5npeg";
        String dbHost = "mysql.bin";
        String database = "Ahclugtenberg";
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            String dbUrl = "jdbc:mysql://" + dbHost + "/" + database;
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        } catch(Exception e){e.printStackTrace();}
    }

    /**
     * Prepared statement to check if an user exists in the database given the username and the password
     * @param name the username of the user trying to log in
     * @param pass the password of the user trying to log in
     * @return boolean if it's an existing user
     */
    public boolean checkExistingUser(String name, String pass) {
        boolean isExistingUser = false;
        String fetchExistingUsers  = "SELECT * FROM Users WHERE user_name = ? AND user_password = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(fetchExistingUsers);
            ps.setString(1, name);
            ps.setString(2, pass);
            ResultSet rs = ps.executeQuery();
            if (rs.isBeforeFirst()) {
                isExistingUser = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExistingUser;
    }

    /**
     * Checks if the username already exists in the database
     * @param username the username to be checked
     * @return boolean whether username already exists in the database
     */
    public boolean checkExistingUsername(String username) {
        boolean isExistingUsername = false;
        String fetchExistingUsername = "SELECT * FROM Users WHERE user_name = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(fetchExistingUsername);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.isBeforeFirst()) {
                isExistingUsername = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExistingUsername;
    }

    /**
     * Inserts new users into the database, with 3 parameters
     * @param name username of the user
     * @param pass password of the user
     * @param eMail email of the user
     */
    public void insertUser(String name, String pass, String eMail) {
        try {
            String insertQuery =
                    "INSERT INTO Users (user_name, user_password, user_email) "
                            + " VALUES (?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(insertQuery);
            ps.setString(1, name);
            ps.setString(2, pass);
            ps.setString(3, eMail);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the connection to the database
     */
    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
