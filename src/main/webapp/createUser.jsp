<%--
  Author: Anouk Lugtenberg
  Date: 10-1-2018
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="includes/login-header.jsp"/>

<div class="container-login100">
    <div class="wrap-login100">
        <div class="login100-form-title" style="background-image: url(images/dna-strand.jpg);">
					<span class="login100-form-title-1">
						DNA Translator
					</span>
        </div>

        <form class="login100-form validate-form" action="createUser.do" method="POST">
            <h4>${requestScope.errorMessage}</h4>
            <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                <span class="label-input100">Username</span>
                <input class="input100" type="text" name="username" placeholder="Enter username">
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                <span class="label-input100">Password</span>
                <input class="input100" type="password" name="password" placeholder="Enter password">
                <span class="focus-input100"></span>
            </div>

                <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                    <span class="label-input100">E-mail</span>
                    <input class="input100" type="email" name="email" placeholder="Enter e-mail">
                    <span class="focus-input100"></span>
                </div>

            <div class="container-login100-form-btn">
                <button class="login100-form-btn">
                    Create account
                </button>
            </div>
        </form>

    </div>
</div>


<form action="createUser.do" method="POST">
    <label for="username_field"> User name: </label>
    <input id="username_field" type="text" name="username" required/> <br/>
    <label for="password_field"> User password: </label>
    <input id="password_field" type="password" name="password" required/><br/>
    <label for="email_field">User email: </label>
    <input id="email_field", type="email", name="email" required/><br/>
    <label class="login_field"> </label>
    <input type="submit" value="OK"/>
</form>
<jsp:include page="includes/login-footer.jsp"/>
