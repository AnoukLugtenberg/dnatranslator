<%--
  Author: ahclugtenberg
  Date: 8-1-18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="includes/header.jsp"/>

<div class="container">
<%--<c:choose>--%>
  <%--<c:when test="${sessionScope.userName != null}">--%>
    <h1>Welcome, ${sessionScope.userName}</h1>

    Please provide your DNA to be translated: <br/>
    <form action="translateDNA.do" method="POST">
      <textarea name="sequence" rows="10" cols="50"></textarea>
      <%--<input id="sequence_field" type="text" name="sequence" required/><br/>--%>
      <label for="plusOne">Frame +1: </label>
      <input type="checkbox" id="plusOne" name="frames" value="1"><br/>
      <label for="plusTwo">Frame +2: </label>
      <input type="checkbox" id="plusTwo" name="frames" value="2"><br/>
      <label for="plusThree">Frame +3: </label>
      <input type="checkbox" id="plusThree" name="frames" value="3"><br/>

      <label for="minusOne">Frame -1: </label>
      <input type="checkbox" id="minusOne" name="frames" value="-1"><br/>
      <label for="minusTwo">Frame -2: </label>
      <input type="checkbox" id="minusTwo" name="frames" value="-2"><br/>
      <label for="minusThree">Frame -3: </label>
      <input type="checkbox" id="minusThree" name="frames" value="-3"><br/>

      <label for="completeSixFrame">complete 6-frame translation: </label>
      <input type="checkbox" id="completeSixFrame" name="frames" value="6"><br/>

      <label for="minORF">Minimal open reading frame length: </label>
      <input type="number" id="minORF" name="minORF" value="0"><br/>

      <input type="submit" value="Submit">
    </form>

    <c:if test="${requestScope.errorMessage != null}">
      ${requestScope.errorMessage}
    </c:if>

    <c:if test="${requestScope.acidSequenceList != null}">
        Sequence: ${requestScope.sequence}
      <TABLE BORDER="1" CELLPADDING="3" CELLSPACING="1">
        <TR>
          <TH>Protein Sequence</TH>
          <TH>Index start codon</TH>
          <TH>Index stop codon</TH>
          <TH>Frame</TH>
        </TR>

      <c:forEach items="${requestScope.acidSequenceList}" var="AcidSequence">
        <TR>
          <TD>${AcidSequence.proteinSequence}</TD>
          <TD>${AcidSequence.indexStartCodon}</TD>
          <TD>${AcidSequence.indexStopCodon}</TD>
          <TD>${AcidSequence.frame}</TD>
        </TR>
      </c:forEach>
      </TABLE>
    </c:if>
      
  <%--</c:when>--%>
  <%--<c:otherwise>--%>
    <%--<c:redirect url="/index.jsp"/>--%>
  <%--</c:otherwise>--%>
<%--</c:choose>--%>
  </div>


<jsp:include page="includes/footer.jsp"/>
