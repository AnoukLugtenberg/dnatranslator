<%--
  Author: Anouk Lugtenberg
  Date: 11-1-2018
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="includes/login-header.jsp"/>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form-title" style="background-image: url(images/dna-strand.jpg);">
					<span class="login100-form-title-1">
						DNA Translator
					</span>
            </div>



            <form class="login100-form validate-form" action="login.do" method="POST">
                ${requestScope.message}
                <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                    <span class="label-input100">Username</span>
                    <input class="input100" type="text" name="username" placeholder="Enter username">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Enter password">
                    <span class="focus-input100"></span>
                </div>

                    <div class="flex-sb-m w-full p-b-30">
                        <div>
                            <a href="createUser.jsp" class="txt1">
                                Create new account
                            </a>
                        </div>
                    </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Login
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
<jsp:include page="includes/login-footer.jsp"/>
