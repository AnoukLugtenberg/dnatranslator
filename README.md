# README #



### What is this repository for? ###

* This repository is for a web-based project, where DNA/RNA sequences are translated to protein sequences.

### How do I get set up? ###

* Clone this repository and run the file 'index.jsp', found under src > main > webapp
* If the database can't connect, you should add your own mySQL. You can do this in file:
src > main > java > DNATranslator > dao > MyDbConnector
* You can create a random DNA sequence to test the application at: http://www.bioinformatics.org/sms2/random_dna.html
* Or you can download a sample file from the download section of this repo.

### Extra information ###

* Javadoc can be found under source > Javadoc
* I am far from finished with this project. 
* So far the functionality is only a 3 reading frame - from the '5 to the 3' strand. And only input through a text-field on the site.
* I am positive about receiving feedback, especially about the setup of my backend. 


### Who do I talk to? ###

* Anouk Lugtenberg
* E-mail: a.h.c.lugtenberg@st.hanze.nl